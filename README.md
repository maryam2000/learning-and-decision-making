# Learning and Decision Making

- This project contains three of my Advance Topics and Computational Neuroscience Course simulation.

- I've uploaded a file for each simulation containing its codes and its project report.

- **Simulation 5:** Modelled learning in classical conditioning paradigms (like extiction, blocking, overshadow) by Rescorla-Wagner rule. Also, simulated learning paradigms regarding uncertainty, joint-uncertainty and unexpected uncertainty situations by Kalman filter implementation (Dayan and Yu 2002)

- **Simulation 6:** The Water Maze, Simulated a simple model of navigation problems using reinforcement learning. Rats are placed in a large pool of milky water and have to swim around until they find a small platform that is submerged slightly below the surface of the water. Implementation Q-learning and TD(lambda) algorithms. This also includes 2 demos of the rat finding its way through water!

- **Simulation 7:** Simulation of evidence accumulation model (Drift Diffusion Model) and studying the relationship between accuracy and 
reaction time (RT) in decision making. Also, implementing the model proposed by Shadlen and Newsome (2001) for the interaction between 
area MT and LIP.
